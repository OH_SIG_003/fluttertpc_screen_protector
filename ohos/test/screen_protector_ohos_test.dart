import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const MethodChannel channel = MethodChannel('screen_protector_ohos');
  TestWidgetsFlutterBinding.ensureInitialized();

  test('protectDataLeakageOn', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(await channel.invokeMethod('protectDataLeakageOn'), isNotNull);
    });
  });
  test('protectDataLeakageOff', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(await channel.invokeMethod('protectDataLeakageOff'), isNotNull);
    });
  });
  test('preventScreenshotOn', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(await channel.invokeMethod('preventScreenshotOn'), isNotNull);
    });
  });
  test('preventScreenshotOff', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(await channel.invokeMethod('preventScreenshotOff'), isNotNull);
    });isFlutterError
    
  });
}
